#include "AFNe.hpp"

int main(int argc, char const *argv[]){
	string cadeia, config;
	cout << "arquivo de config: ";
	setbuf(stdin,NULL);
	getline(cin,config);
	AFNe afn(config);
	afn.printEstados();
	afn.printInicial();
	afn.printFinais();
	afn.printRegras();
	vector<string> teste = afn.efecho("q0");
	cout << "efecho(q0): ";
	for(uni i=0; i<teste.size(); i++) cout << teste.at(i) << " ";
	cout << "\n";
	while(true){
		afn.reset();
		cout << "digite a cadeia:";
		setbuf(stdin,NULL);
		getline(cin,cadeia);
		if(afn.aceitacao(cadeia)) cout << "Cadeia ACEITA!\n";
		else cout << "Cadeia REJEITADA!\n";
	}
	return 0;
}