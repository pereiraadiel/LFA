#include "AFN.hpp"

int main(int argc, char const *argv[]){
	string cadeia, config;
	cout << "arquivo de config: ";
	setbuf(stdin,NULL);
	getline(cin,config);
	AFN afn(config);
	afn.printEstados();
	afn.printInicial();
	afn.printFinais();
	afn.printRegras();
	while(true){
		afn.reset();
		cout << "digite a cadeia:";
		setbuf(stdin,NULL);
		getline(cin,cadeia);
		if(afn.aceitacao(cadeia)) cout << "Cadeia ACEITA!\n";
		else cout << "Cadeia REJEITADA!\n";
	}
	return 0;
}