#ifndef AFN_HPP
#define AFN_HPP
	#include <iostream>
	#include <fstream>
	#include <vector>

	using namespace std;
	typedef unsigned int uni;

	class AFN{
		string estadoInicial;
		string estadoAtual;
		vector<string> estados;
		vector<string> estadosFinais;
		vector<string> transicao;
		vector<vector<string>> atingido;
		public:
			AFN(string caminho);
			~AFN();
			// configuracao do automato finito deterministico
			void reset(); // resetar estadoAtual
			void set_estados(string); // configurar estados
			void set_estado_inicial(string); // configurar estadoInicial
			void set_estados_finais(string); // configurar estadosFinais
			void set_regras(string); // configurar Regras
			// verificacao de aceitacao
			bool aceitacao(string);

			// SOMENTE PARA DEPURACAO
			void printEstados();
			void printInicial();
			void printFinais();
			void printRegras();

	};
#endif // AFN_HPP