#include "AFN.hpp"

AFN::AFN(string caminho){
	string line;
	ifstream arquivo(caminho);
	if(arquivo.is_open()){
		getline(arquivo,line);
		set_estados(line);
		getline(arquivo,line);
		set_estado_inicial(line);
		getline(arquivo,line);
		set_estados_finais(line);
		while(!arquivo.eof()){
			getline(arquivo,line);
			set_regras(line);
		}
		arquivo.close();
	}
}

AFN::~AFN(){

}

void AFN::reset(){
	estadoAtual = estadoInicial;
}

void AFN::set_estados(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==',' || linha.at(i)==';'){
			estados.push_back(aux);
			aux = "";
		}
		else aux += linha.at(i);
	}
}

void AFN::set_estado_inicial(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==';'){
			estadoInicial = aux;
			aux = "";
			return;
		}
		else aux += linha.at(i);
	}
}

void AFN::set_estados_finais(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==',' || linha.at(i)==';'){
			estadosFinais.push_back(aux);
			aux = "";
		}
		else aux += linha.at(i);
	}
}

void AFN::set_regras(string linha){
	string aux;
	string transit;
	string atingiu;
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)=='>'){
			transit = aux;
			aux="";
		}
		else if(linha.at(i)==';'){
			atingiu = aux;
			atingiu+=';';
			aux = "";
			break;
		}
		else aux += linha.at(i);
	}
	if(transicao.size() == atingido.size()){
		string novoEstado;
		vector<string> states;
		for(uni j=0; j<atingiu.length(); j++){
			if(atingiu.at(j)=='+' || atingiu.at(j)==';'){
				states.push_back(novoEstado);
				novoEstado = "";
			}
			else novoEstado += atingiu.at(j);
		}
		transicao.push_back(transit);
		atingido.push_back(states);
	}
	else {
		cout << "ERRO: Falha ao configurar as regras\n";
		exit(1);
	}
}

bool AFN::aceitacao(string cadeia){
	if(cadeia.length()==0){
		for(uni i=0; i<estadosFinais.size(); i++){
			if(estadosFinais.at(i) == estadoAtual) return true;
		}
		return false;
	}
	string transit = "";
	transit += estadoAtual;
	transit += ',';
	transit += cadeia.at(0);
	bool aceita = false;
	for(uni i=0; i<transicao.size(); i++){
		if(transit == transicao.at(i)){
			//estadoAtual = atingido.at(i);
			string anterior;
			for(uni j=0; j<atingido.at(i).size(); j++){
				anterior = estadoAtual;
				estadoAtual = atingido.at(i).at(j);
				//cout << "estadoAtual: " << estadoAtual << "\n";
				aceita = aceitacao(cadeia.substr(1,cadeia.length()));
				if(aceita) break;
				else {
					cout << "backtraking\n";
					estadoAtual = anterior;
				}
			}
			if(aceita) break;
		}
	}
	return aceita;
}

//SOMENTE PARA DEPURACAO:

void AFN::printEstados(){
	cout << "ESTADOS: ";
	for(uni i=0; i<estados.size(); i++){
		cout << "\'" << estados.at(i) << "\' ";
	}
	cout << "\n";
}

void AFN::printInicial(){
	cout << "ESTADO INICIAL: " << estadoInicial << "\n";
}

void AFN::printFinais(){
	cout << "ESTADOS FINAIS: ";
	for(uni i=0; i<estadosFinais.size(); i++){
		cout << "\'" << estadosFinais.at(i) << "\' ";
	}
	cout << "\n";
}

void AFN::printRegras(){
	if(transicao.size() != atingido.size()) {
		cout << "ERRO: transicao.size() diferente de atingido.size()\n";
		exit(1);
	}
	cout << "REGRAS:\n";
	for(uni i=0; i<transicao.size(); i++){
		cout << transicao.at(i) << "->";
		for(uni j=0; j<atingido.at(i).size(); j++){
			cout << atingido.at(i).at(j) << " ";
		}
		cout << "\n";
	}
}