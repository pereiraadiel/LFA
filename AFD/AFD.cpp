#include "AFD.hpp"

AFD::AFD(string caminho){
	string line;
	ifstream arquivo(caminho);
	if(arquivo.is_open()){
		getline(arquivo,line);
		set_estados(line);
		getline(arquivo,line);
		set_estado_inicial(line);
		getline(arquivo,line);
		set_estados_finais(line);
		while(!arquivo.eof()){
			getline(arquivo,line);
			set_regras(line);
		}
		arquivo.close();
	}
}

AFD::~AFD(){

}

void AFD::reset(){
	estadoAtual = estadoInicial;
}

void AFD::set_estados(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==',' || linha.at(i)==';'){
			estados.push_back(aux);
			aux = "";
		}
		else aux += linha.at(i);
	}
}

void AFD::set_estado_inicial(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==';'){
			estadoInicial = aux;
			aux = "";
			return;
		}
		else aux += linha.at(i);
	}
}

void AFD::set_estados_finais(string linha){
	string aux = "";
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)==',' || linha.at(i)==';'){
			estadosFinais.push_back(aux);
			aux = "";
		}
		else aux += linha.at(i);
	}
}

void AFD::set_regras(string linha){
	string aux;
	string transit;
	string atingiu;
	for(uni i=0; i<linha.length(); i++){
		if(linha.at(i)=='>'){
			transit = aux;
			aux="";
		}
		else if(linha.at(i)==';'){
			atingiu = aux;
			aux = "";
			break;
		}
		else aux += linha.at(i);
	}
	if(transicao.size() == atingido.size()){
		transicao.push_back(transit);
		atingido.push_back(atingiu);
	}
	else {
		cout << "ERRO: Falha ao configurar as regras\n";
		exit(1);
	}
}

bool AFD::aceitacao(string cadeia){
	if(cadeia.length()==0){
		for(uni i=0; i<estadosFinais.size(); i++){
			if(estadosFinais.at(i) == estadoAtual) return true;
		}
		return false;
	}
	string transit = "";
	transit += estadoAtual;
	transit += ',';
	transit += cadeia.at(0);
	for(uni i=0; i<transicao.size(); i++){
		if(transit == transicao.at(i)){
			estadoAtual = atingido.at(i);
			break;
		}
	}
	return aceitacao(cadeia.substr(1,cadeia.length()));
}


//SOMENTE PARA DEPURACAO:

void AFD::printEstados(){
	cout << "ESTADOS: ";
	for(uni i=0; i<estados.size(); i++){
		cout << "\'" << estados.at(i) << "\' ";
	}
	cout << "\n";
}

void AFD::printInicial(){
	cout << "ESTADO INICIAL: " << estadoInicial << "\n";
}

void AFD::printFinais(){
	cout << "ESTADOS FINAIS: ";
	for(uni i=0; i<estadosFinais.size(); i++){
		cout << "\'" << estadosFinais.at(i) << "\' ";
	}
	cout << "\n";
}

void AFD::printRegras(){
	if(transicao.size() != atingido.size()) {
		cout << "ERRO: transicao.size() diferente de atingido.size()\n";
		exit(1);
	}
	cout << "REGRAS:\n";
	for(uni i=0; i<transicao.size(); i++){
		cout << transicao.at(i) << "->" << atingido.at(i) << "\n";
	}
}