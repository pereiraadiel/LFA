#include "AFD.hpp"

int main(int argc, char const *argv[]){
	string cadeia, config;
	cout << "arquivo de config: ";
	setbuf(stdin,NULL);
	getline(cin,config);
	AFD afd(config);
	afd.printEstados();
	afd.printInicial();
	afd.printFinais();
	afd.printRegras();
	while(true){
		afd.reset();
		cout << "digite a cadeia:";
		setbuf(stdin,NULL);
		getline(cin,cadeia);
		if(afd.aceitacao(cadeia)) cout << "Cadeia ACEITA!\n";
		else cout << "Cadeia REJEITADA!\n";
	}
	return 0;
}